FROM fedora:latest

ARG USERNAME=linter

RUN adduser ${USERNAME}

RUN dnf -y install python python-pip && \
     dnf clean all
     
USER ${USERNAME}
RUN pip install --no-cache-dir j2lint && \
    pip install --no-cache-dir pylint && \
    pip cache purge

ENV PATH="/home/${USERNAME}/.local/bin:${PATH}"
